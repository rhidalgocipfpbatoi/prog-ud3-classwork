
package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat5 {
    
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Dime tu nota:");
        int nota = scanner.nextInt();
        
        switch (nota) {
            case 0, 1, 2 -> System.out.println("MUY DEFICIENTE");
            case 3, 4 -> System.out.println("INSUFICIENTE");
            case 5 -> System.out.println("SUFICIENTE");     
            case 6 -> System.out.println("BIEN");
            case 7, 8 -> System.out.println("NOTABLE");    
            case 9, 10 -> System.out.println("SOBRESALIENTE");
                
            default -> System.out.println("INCORRECTO");
                        
        }      
    }
    
}
