
package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat4 {
    
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Dime tu nota:");
        int nota = scanner.nextInt();
        
        switch (nota) {
            case 0:
            case 1:
            case 2:
                System.out.println("MUY DEFICIENTE");
                break;
            
            case 3:
            case 4:
                System.out.println("INSUFICIENTE");
                break;
            
            case 5:
                System.out.println("SUFICIENTE");
                break;
                
            case 6:
                System.out.println("BIEN");
                break;
                
            case 7:
            case 8:
                System.out.println("NOTABLE");
                break;
                
            case 9:
            case 10:
                System.out.println("SOBRESALIENTE");
                break;
            
            default:
                 System.out.println("INCORRECTO");
                        
        }      
    }
    
}
