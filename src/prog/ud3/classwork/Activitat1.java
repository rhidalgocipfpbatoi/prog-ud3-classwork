package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat1 {
    
    static final int ANYOS_MAYORIA_EDAD = 18;
            
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Dime tu edad:");
        
        byte edad = scanner.nextByte();
        
        if (edad >= ANYOS_MAYORIA_EDAD) {
            System.out.println("Eres mayor de edad");
        }
        else {
            System.out.println("Eres menor de edad");
        }
    }
}
