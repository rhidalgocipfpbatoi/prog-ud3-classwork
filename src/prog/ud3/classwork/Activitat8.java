/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat8 {
    
    static final int CUOTA_GENERAL = 500;
    static final float DESCUENTO_MAYORES_DE_65 = 0.45f;
    static final float DESCUENTO_MENORES_DE_18 = 0.25f;
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Introduce nombre:");
        String nombre = scanner.next();
        System.out.print("Introduce edad:");
        int edad = scanner.nextInt();
        float aPagar;
        
        if (edad < 0 || edad > 120) {
            System.out.println("Edad incorrecta");
        } else {
            if (edad > 65) {
                aPagar = CUOTA_GENERAL - CUOTA_GENERAL * DESCUENTO_MAYORES_DE_65;
            } else if (edad > 0 && edad < 18) {
                aPagar = CUOTA_GENERAL - CUOTA_GENERAL * DESCUENTO_MENORES_DE_18;
            } else {
                aPagar = CUOTA_GENERAL;
            }

            System.out.printf("%s, tienes que pagar %.1f euros.", nombre, aPagar);
        }
        
    }
}
