
package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat20 {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Validación número está entre 1 y 10
        int numero;
        
        do {
            System.out.print("Escribe un entero: ");
            numero = scanner.nextInt();
        } while(numero < 1 || numero > 10);
        
        // Mostrar la tabla de multiplicar
        
        System.out.printf("Tabla del %d\n", numero);
        System.out.println("------------");
        /*int contador = 1;
        do {
            int resultado = numero * contador;
            System.out.printf("%d X %d = %d\n", 
                    numero, contador, resultado);
            contador++;
        }while(contador <= 10);*/
        
        for (int i = 1; i <= 10; i++) {
            System.out.printf("%d X %d = %d\n", numero, i, numero * i);
        }
    }
}
