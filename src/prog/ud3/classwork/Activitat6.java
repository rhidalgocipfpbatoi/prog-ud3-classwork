package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat6 {
    
    static final int OPCION_PLASTICO = 1;
    static final int OPCION_ORGANICO = 2;
    static final int OPCION_PAPEL = 3;
    static final int OPCION_CARTON = 4;
    static final int OPCION_OTROS = 5;
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("¿Qué tipo de desecho quieres tirar?");
        System.out.printf("%d.Plástico\n", OPCION_PLASTICO);
        System.out.printf("%d.Orgánico\n", OPCION_ORGANICO);
        System.out.printf("%d.Papel\n", OPCION_PAPEL);
        System.out.printf("%d.Cartón\n", OPCION_CARTON);
        System.out.printf("%d.Otros\n", OPCION_OTROS);
        System.out.printf("Introduce una opción[%d-%d]:", 
                OPCION_PLASTICO, OPCION_OTROS);
        
        int opcion = scanner.nextInt();
        
        switch (opcion) {
            case OPCION_PLASTICO:
                System.out.println("Debes tirarlo al contenedor Amarillo");
                break;
            case OPCION_ORGANICO:
                System.out.println("Debes tirarlo al contenedor Gris");
                break;
            case OPCION_PAPEL:
            case OPCION_CARTON:
                System.out.println("Debes tirarlo al contenedor Azul");
                break;
            case OPCION_OTROS:
                System.out.println("Debes tirarlo al contenedor  Verde");
                break;
            default:
                System.out.println("Opción incorrecta");
        }
                
    }
}
