package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat17 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int numero;
        
        do {
            System.out.print("Introduce un número: ");
            numero = scanner.nextInt();
            System.out.println(numero);
        }while(numero != 10);
    }
}
