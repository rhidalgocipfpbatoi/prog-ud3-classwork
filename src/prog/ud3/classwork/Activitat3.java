/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat3 {
    
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Dime tu nota:");
        float nota = scanner.nextFloat();
        
        if (nota < 0 || nota >10) {
            System.out.println("INCORRECTO");
        }else if (nota >= 0 && nota < 3) {
            System.out.println("MUY DEFICIENTE");
        } else if (nota >= 3 && nota < 5) {
            System.out.println("INSUFICIENTE");
        } else if (nota >= 5 && nota < 6) {
            System.out.println("SUFICIENTE");
        } else if (nota >=6 && nota < 7) {
            System.out.println("BIEN");
        } else if (nota >=7 && nota < 9) {
            System.out.println("NOTABLE");
        } else {
            System.out.println("SOBRESALIENTE");
        } 
       
    }
    
}
