package prog.ud3.classwork;

/**
 *
 * @author batoi
 */
public class Activitat14 {
    public static void main(String[] args) {
        int contador = 10;
        
        while(contador >= 0) {
            System.out.println(contador);
            contador -= 1;
        }
        System.out.println("Preparando la partida...");
    }
}
