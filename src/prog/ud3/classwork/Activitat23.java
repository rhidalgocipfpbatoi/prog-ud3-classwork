
package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat23 {
    
    final static int MAXIMO = 50;
    
    public static void main(String[] args) {
        
        int numPares = 0;
        int numImpares = 0;
        int numMultiplos5 = 0;
        int numParesMultiplos5 = 0;
        Scanner scanner = new Scanner(System.in);
        
        for (int i = 1; i <= MAXIMO; i++) {
            System.out.print("Introduce un número:");
            int numero = scanner.nextInt();
            
            if (numero == 100) {
                continue;
            } else if (numero == 0) {
                break;
            } else {
            
                if (numero % 2 == 0) {
                    numPares++;

                    if (numero % 5 == 0) {
                        numParesMultiplos5++;
                    }
                } else {
                    numImpares++;
                }

                if (numero % 5 == 0) {
                    numMultiplos5++;
                }
            }
        }
        System.out.printf("Hay %d pares\n" , numPares);
        System.out.printf("Hay %d impares\n" , numImpares);
        System.out.printf("Hay %d múltiplos de 5\n" , numMultiplos5);
        System.out.printf("Hay %d pares y múltiplos de 5\n" , numMultiplos5);
    }
}
