
package prog.ud3.classwork;

/**
 *
 * @author batoi
 */
public class Activitat22 {
    public static void main(String[] args) {
        // Mostrar la tabla de multiplicar
        
        for (int i = 1; i <= 10; i++) {
            System.out.printf("Tabla del %d\n", i);
            System.out.println("------------");

            for (int j = 1; j <= 10; j++) {
                System.out.printf("%d X %d = %d\n", i, j, i * j);
            }
        }
        
    }
}
