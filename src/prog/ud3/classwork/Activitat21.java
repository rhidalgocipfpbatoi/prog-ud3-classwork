
package prog.ud3.classwork;

/**
 *
 * @author batoi
 */
public class Activitat21 {
    
    final static int MAXIMO = 99;
    
    public static void main(String[] args) {
        
        int numPares = 0;
        int numMultiplos5 = 0;
        int numParesMultiplos5 = 0;
        
        for (int i = 1; i <= MAXIMO; i++) {
            if (i % 2 == 0) {
                numPares++;
                
                if (i % 5 == 0) {
                    numParesMultiplos5++;
                }
            }
            
            if (i % 5 == 0) {
                numMultiplos5++;
            }
        }
        System.out.printf("Hay %d pares\n" , numPares);
        System.out.printf("Hay %d impares\n" , MAXIMO - numPares);
        System.out.printf("Hay %d múltiplos de 5\n" , numMultiplos5);
        System.out.printf("Hay %d pares y múltiplos de 5\n" , numMultiplos5);
    }
}
