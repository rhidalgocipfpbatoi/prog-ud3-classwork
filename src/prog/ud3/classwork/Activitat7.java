/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud3.classwork;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat7 {
    public static void main(String[] args) {
        int numero1;
        int numero2;
        String mensaje;
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Escribe un entero:");
        numero1 = scanner.nextInt();
        
        System.out.print("Escribe otro:");
        numero2 = scanner.nextInt();
        
        /*if (numero1 > numero2) {
            mensaje = "Número 1 es mayor";
        } else {
            mensaje = "Número 2 es mayor o son iguales";
        }*/
        
        mensaje = (numero1 > numero2) 
                ? "Número 1 es mayor"
                : "Número 2 es mayor o son iguales";
        
        System.out.println(mensaje);
    }
}
